all:
	pdflatex slides.tex   -interaction=nonstopmode
indexes:
	-pdflatex slides  -interaction=nonstopmode
	-bibtex slides
	-makeglossaries slides
	-pdflatex slides  -interaction=nonstopmode
clean:
	git add *.png
	git clean -fx
again: clean
again: indexes
